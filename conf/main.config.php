<?
    $env = getenv('HOST_ENVIRONMENT');
    switch ($env) {
        case 'LOCAL':
            define('DATE',                  date('Y-m-d H:i:s'));
            define('DEBUG_STYLE',           'background-color: #ffff99; font-size: 8pt; clear: both; text-align: left;');

            define('DB_HOST',               'db host');
            define('DB_NAME',               'db_name');
            define('DB_USER',               'db user');
            define('DB_PASSWORD',           'db pass');
            define('DB_CHARSET',            'utf8');
            define('DB_COLLATE',            'utf8_general_ci');

            define('AUTH_KEY',              '');
            define('SECURE_AUTH_KEY',       '');
            define('LOGGED_IN_KEY',         '');
            define('NONCE_KEY',             '');
            define('AUTH_SALT',             '');
            define('SECURE_AUTH_SALT',      '');
            define('LOGGED_IN_SALT',        '');
            define('NONCE_SALT',            '');

            define('WP_DEBUG',              true);
            break;
        default:
            define('DATE',                  date('Y-m-d H:i:s'));
            define('DEBUG_STYLE',           'background-color: #ffff99; font-size: 8pt; clear: both; text-align: left;');

            define('DB_HOST',               'db host');
            define('DB_NAME',               'db name');
            define('DB_USER',               'db user');
            define('DB_PASSWORD',           'db pass');
            define('DB_CHARSET',            'utf8');
            define('DB_COLLATE',            'utf8_general_ci');

            define('AUTH_KEY',              '');
            define('SECURE_AUTH_KEY',       '');
            define('LOGGED_IN_KEY',         '');
            define('NONCE_KEY',             '');
            define('AUTH_SALT',             '');
            define('SECURE_AUTH_SALT',      '');
            define('LOGGED_IN_SALT',        '');
            define('NONCE_SALT',            '');

            define('WP_DEBUG',              false);
            break;
    }
?>