<?php
    require_once(realpath(dirname(__FILE__) . '/../conf/main.config.php'));

    /**
     * Set the default theme to our own instead of the annual
     * named themes that come with WordPress.
     * 
     * NOTE: If you change this value you will also need to
     *       change the name of the theme directory.
     */
    define('WP_DEFAULT_THEME', 'main');

    // disable php file editing for better security
    define('DISALLOW_FILE_EDIT', true);

    // database charset and collate type to use in creating database tables
    define('DB_CHARSET', 'utf8');
    define('DB_COLLATE', 'utf8_general_ci');

    // WordPress Database Table prefix
    $table_prefix = 'wp_';

    // get the absolute path to the WordPress directory and setup the vars and includes
    if (!defined('ABSPATH')) define('ABSPATH', dirname(__FILE__) . '/');
    require_once(ABSPATH . 'wp-settings.php');
?>