<?php
    /**
     * Main
     *
     * This is the main class for the theme.  It uses the Main namespace
     * so accessing its methods can be done like:
     * 
     *   Main\Theme::some_method()
     */
    namespace Main;

    class Theme
    {
        /**
         * init
         *
         * Entry point for the theme.  Setting up hooks, shortcodes, and
         * any common theme variables should be done here.
         */
        public static function init()
        {
            // turn off password reset and email change messages
            add_filter('send_password_change_email', '__return_false');
            add_filter('send_email_change_email',    '__return_false');

            // remove the default canonical links and shortlinks
            remove_action('wp_head', 'rel_canonical',        10, 0);
            remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

            // shortcodes
            add_shortcode('testcode', array('Main\Theme', 'sc_test'));
        }

        /**
         * sc_test
         *
         * Test shortcode for illustrative purposes.
         *
         * @param   array   $atts     array of named params
         * @param   string  $content  inner content if used as a tag
         * @return  string  $out      shortcode output
         */
        public static function sc_test($atts, $content=null)
        {
            $a = shortcode_atts(
                array(
                    'content' => null
                ),
                $atts
            );

            $out = sprintf(
                '<span class="shortcode-test">%s</span>',
                $a['content']
            );

            return($out);
        }
    }

    // do it!
    Theme::init();
?>