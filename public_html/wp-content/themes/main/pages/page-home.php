<?php
    /**
     * Template Name: Home Page
     *
     * The home page.
     */

    // get the contents of the post content text area and filter it for shortcodes and such
    $content = apply_filters('the_content', get_post_field('post_content', null));

    // get the header
    get_header();
?>

<div id="wrapper">

    <div id="header">
        <h1>Your Site Title</h1>
    </div><!-- /header -->

    <div id="main">

        <div id="content">
            <h2>Your Content Title: <? the_title(); ?></h2>
            <p>Your content: <?= $content ?></p>
        </div><!-- /content -->

        <div id="sidebar">
            <p>Your sidebar content...</p>
        </div><!-- /sidebar -->

    </div><!-- /main -->

    <div id="footer">
        <p>Your footer content...</p>
    </div><!-- /footer -->

</div><!-- /wrapper -->

<?php get_footer(); ?>